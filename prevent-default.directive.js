(function (){
    'use strict';

    function preventDefault ($location) {
        return {
            restrict: "EAC",
            
            link: function(scope, element){
                element.bind('click', function(event){
                    event.preventDefault();
                    $location.path(element.attr('href')).replace();
                });
            }
        };
    }

    preventDefault.$inject = ['$location'];

    angular
        .module('bravoureAngularApp').
        directive('preventDefault', preventDefault);

})();
