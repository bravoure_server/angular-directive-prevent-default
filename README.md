# Bravoure - Angular Directive Prevent Default

## Use

This Component is used to prevent a link to be reloading the page
 
### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-directive-prevent-default": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
